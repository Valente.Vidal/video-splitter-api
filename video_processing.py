import MySQLdb
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
from settings_local import *
from datetime import datetime
import requests
import boto3
import os


class Video_processor:

    db = MySQLdb.connect(host=HOST, port=PORT, user=USER, passwd=PASSWORD, db=DBNAME, autocommit=True)
    cursor = db.cursor()
    
    def __init__(self):
        self.video_pk = None
        self.video_id = None
        self.video_dir = None
        self.start_time = None
        self.end_time = None
        self.max_end_time = None
        self.edited_video_dir = None
        self.s3_download_url = None
        self.running = True


    def get_video_pk(self):

        """
        gets the pk of the first video that has not been processed
        """
        self.cursor.execute("SELECT id FROM video_information WHERE processed = 0 ORDER BY expire_by ASC LIMIT 1")
        row = self.cursor.fetchone()
        if row != None:
            self.video_pk = row[0]
            #print (self.video_pk)

    def get_video_info(self):

        """
        get the video info from the latest video_pk 
        """
        if self.video_pk != None:
            self.cursor.execute(f"SELECT video_id, start_time, end_time from video_information WHERE id = {self.video_pk}")
            info = (self.cursor.fetchone())
            self.video_id = info[0]
            (h, m, s) = info[1].split(':')
            self.start_time = int(h) * 3600 + int(m) * 60 + int(s)
            (h, m, s) = info[2].split(':')
            self.end_time = int(h) * 3600 + int(m) * 60 + int(s)
        else:
            print ("No Video PK")
    
    def download_video(self):
        """
        downloads the video and names it 
        """
        if self.video_id != None:
            timestamp = now = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
            video_url = f'http://85072-c.ooyala.com/{self.video_id}/DOcJ-FxaFrRg4gtDEwOjIwbTowODE7WK'
            print ("Downloading...")
            response = requests.get(video_url)
            video_name = f'{ROOT_DIR}/{timestamp}-{self.video_id}.mp4'
            with open(video_name, 'wb') as f:
                f.write(response.content)
            print ("Finished Downloading!")
            self.video_dir = video_name
        else:
            print ("No Video ID")

    def video_length_check(self):
        """
        This is something I couldn't get around to do because of lack of time
        TODO this would have checked the length of the video against the time 
        provided
        """
        pass        
        

    def edit_video(self):
        """
        cuts out the desired piece of the video
        """
        edited_video_dir = self.video_dir.replace(".mp4", "-edited.mp4")
        ffmpeg_extract_subclip(self.video_dir, self.start_time, self.end_time, targetname=edited_video_dir)
        self.edited_video_dir = edited_video_dir


    def upload_video(self):
        """
        uploads the video to the s3 bucket 
        """
        s3 = boto3.client('s3', aws_access_key_id=AWSAccessKeyId, aws_secret_access_key=AWSSecretKey)

        s3_file = self.edited_video_dir.replace(ROOT_DIR, "")
        try:
            s3.upload_file(self.edited_video_dir, BUCKET, s3_file, ExtraArgs={'ACL':'public-read'})
            print("Upload Successful")
            self.s3_download_url = f"https://hopster-valentefv.s3.eu-west-2.amazonaws.com/{s3_file}"
            return True
        except FileNotFoundError:
            print("The file was not found")
            return False
        except NoCredentialsError:
            print("Credentials not available")
            return False

    def delete_temp_files(self):
        os.remove(self.video_dir)
        os.remove(self.edited_video_dir)
        print("Temp Files Removed!")
    
    def update_row(self):
        sql = f"UPDATE video_information SET processed = 1, s3_download_url = '{self.s3_download_url}' WHERE id = {self.video_pk}"
        self.cursor.execute(sql)        

    def coordinator(self):
        try:
            #print ("Getting Video PK")
            self.get_video_pk()
            #print ("Getting Video Info")
            self.get_video_info()
            #print ("Downloading Video")
            self.download_video()
            #print ("Editing Video")
            self.edit_video()
            #print ("Uploading Video")
            self.upload_video()
            #print ("Deleting Temp Files")
            self.delete_temp_files()
            #print ("Updating S3 Download Link")
            self.update_row()
            print ("Finish")
            self.db.close()
        except Exception as e:
            print (e)
            


if __name__ == "__main__":
    a = Video_processor()
    a.coordinator()






