
# video-splitter-api

  

Simple API that takes three parameters; video ID or full URL, start time and end time, and returns a URL where you can download the video splitted in the desired times once it has finished processing.

After calling the end point it will return with a download url, if the video is done processing, the URL will automatically redirect to download the video, if the video hasn't been processed it will redirect to a page notifying you to wait a couple of mins.

  

## End Points

  

video_id can be full link or just the video ID  

http://35.178.58.157/api/submit-video
takes a post request in the following format


  

`{`

`"video_id" : "k3YmNzaTE6XAeocyVuctpmcXe4g_iNWh",`

`"start_time" : "00:00:01"  `

`"end_time" : "00:00:10"  `

`}`  

  
A simple way to test it is to use httpie  

To install httpie;  

`pip install httpie`  

To use httpie;  

`http POST http://35.178.58.157/api/submit-video video_id=k3YmNzaTE6XAeocyVuctpmcXe4g_iNWh start_time=00:00:01 end_time=00:00:15`  

  
  

## Front Page

There is also a front page where one can submit a video id or video URL, start time and end time

Currently there is a crontab running every min so one shouldn't wait more than a minute to get a downloadable link

[http://35.178.58.157/](http://35.178.58.157/)

  
  
  

## Installation Notes

If you're in **Windows** and are having trouble installing mysqlclient please download the wheel separately from [https://www.lfd.uci.edu/~gohlke/pythonlibs/#mysqlclient](https://www.lfd.uci.edu/~gohlke/pythonlibs/#mysqlclient) for your system

  

in **Ubuntu**

remove "mysqlclient==1.4.6" from requirements

run; "pip install mysql-connector"

and then run "pip install -r requirements.txt"