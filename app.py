#!flask/bin/python
from flask import Flask, render_template, request, jsonify, redirect
from flask_mysqldb import MySQL
from settings_local import *
from datetime import datetime, timedelta
import requests
import uuid

app = Flask(__name__)


# MySQL configurations
app.config['MYSQL_USER'] = USER
app.config['MYSQL_PASSWORD'] = PASSWORD
app.config['MYSQL_DB'] = DBNAME
app.config['MYSQL_HOST'] = HOST
app.config['MYSQL_PORT'] = PORT
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
app.config['DEBUG'] = DEBUG
app.config['TEMPLATES_AUTO_RELOAD'] = TEMPLATES_AUTO_RELOAD
mysql = MySQL(app)


def video_url_check(video_id):
    """
    This function checks if a video id or a full video url is 
    provided, if it has it returns back the video id and 
    a response if its valid url or not
    """

    # check if url is full or just id 
    # since I only had one example url I 
    # assumed the domain and sub-domain 
    # would remain the same
    domain = "http://85072-c.ooyala.com/"

    # checks if the url is a full link
    if domain in video_id:
        video_id = video_id.replace(domain, "")
        if video_id.count('/') > 1:
            return "URL not working"
        (video_id, second_part) = video_id.split("/")  
        video_url = f'http://85072-c.ooyala.com/{video_id}/DOcJ-FxaFrRg4gtDEwOjIwbTowODE7WK'
        # checks if the url is live
        r = requests.head(video_url)
        if r.status_code == 200:
            return  "Valid URL", video_id
        else:
            return "URL not working"
    else:
        # checks if the url provided is a video id only
        # I'm assuming all video ids are 32 characters
        if len(video_id) == 32:
            video_url = f'http://85072-c.ooyala.com/{video_id}/DOcJ-FxaFrRg4gtDEwOjIwbTowODE7WK'
            r = requests.head(video_url)
            if r.status_code == 200:
                return "Valid URL", video_id
            else:
                return "URL not working"


def save_info(video_id, start_time, end_time, download_url):
    """
    Saves video id, start time and end time to the database 
    """
    # I added a expire by time so the videos wouldn't stay on s3
    # forever TODO but this has not been implemented yet
    expire_by = datetime.now() + timedelta(hours=12)
    cur = mysql.connection.cursor()
    cur.execute(f''' INSERT INTO video_information (video_id, start_time, end_time, download_url, expire_by) VALUES ('{video_id}', '{start_time}', '{end_time}', '{download_url}', '{expire_by}')''')
    mysql.connection.commit()



@app.route('/<download_url>', methods=['GET'])
def download_url(download_url):
    """
    Queries the database if the video has been processed
    if it has it gives the video download link
    if it hasn't it redirects to a 'please try again later'
    """
    cur = mysql.connection.cursor()
    full_download_url = DOMAIN + download_url
    cur.execute (f"SELECT processed, s3_download_url from video_information WHERE download_url = '{full_download_url}'")
    data = cur.fetchone()

    if data['processed'] == 1:
        return redirect(data['s3_download_url'], code=301)
    elif data['processed'] == 0:
        return render_template('processing.html',code=200)
        

@app.route('/api/submit-video', methods=['POST'])
@app.route('/submit-video', methods=['POST'])
def submit_video():
    """
    This is the API 
    Depending on the route it will return 
    a template or a json response
    """
    try:
        if request.form:
            data = request.form.to_dict()
            _video_id = data['video id']
            # when submiting from website its 
            # missing hour in the format
            _start_time = "00:" + data['start time']
            _end_time = "00:" + data['end time']
        if request.json:
            _video_id = request.json['video_id']
            _start_time = request.json['start_time']
            _end_time = request.json['end_time']

        # validate the received values TODO I'm aware this could be removed
        if _video_id and _start_time and _end_time and request.method == 'POST':
 
            # processes the url proviced
            check_resp, _video_id = video_url_check(_video_id)

            if check_resp == "Valid URL":               
                download_url = f"{DOMAIN}{uuid.uuid4()}" 


                save_info(_video_id, _start_time, _end_time, download_url)
            

            if request.path == '/api/submit-video':
                    resp = jsonify(f"Valid URL, processing video, download url: {download_url} ")
                    resp.status_code = 200
                    return resp

            if request.path == '/submit-video':
                return render_template('success.html', link=download_url)


            else:
                resp = jsonify("Url Not Valid, Please check video id and try again")
                resp.status_code = 500
                return resp
        else:
            return "Error!!"






    except Exception as e:
        print (e)
        resp = jsonify("Error!")
        resp.status_code = 500
        return resp



@app.route('/')
def index():
    return render_template('home.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0')